package com.sonnglung.challenges;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
	    format = "pretty",
	    features = "features"  
	)
public class TestRun {

}
