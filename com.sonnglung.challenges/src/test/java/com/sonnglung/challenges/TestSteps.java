package com.sonnglung.challenges;

import org.openqa.selenium.WebDriver;

import com.sonnglung.challengeweb.checkList;
import com.sonnglung.challengeweb.pageLogin;
import com.sonnglung.challengeweb.registerUser;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestSteps {
	
    WebDriver driver;
    registerUser register;
	@Given("^logado no sistema$")
	
	public void logado_no_sistema() throws Throwable {
        pageLogin myLogin;
        myLogin= new pageLogin();
        myLogin.getDriver();
        this.driver = myLogin.driver;
        myLogin.executeLogin("admin@phptravels.com","demoadmin");

	}

	@When("^registrar novo supplier$")
	public void registrar_novo_supplier() throws Throwable {
        registerUser regist = new registerUser(driver);
        regist.fillFields();
        regist.submitRegister();
        this.register = regist;

	}

	@Then("^supplier deve estar no topo da lista$")
	public void supplier_deve_estar_no_topo_da_lista() throws Throwable {
        String userFName = register.getFirstName();
        String userLName = register.getLastName();
        checkList check = new checkList(register.driver);
        check.assignName(userFName, userLName);
        boolean result = check.chekInList();
        driver.close();
        if (result == false) {
        	throw new PendingException();
        }
	}
}
