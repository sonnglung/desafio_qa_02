package challengeapi;

import com.sonnglung.challengeweb.fakeUser;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class apiSteps {
	apiPOST post = new apiPOST();
	int result;
	
	@Given("^enviada requisicao POST$")
	public void enviada_requisicao_POST() throws Throwable {
		fakeUser user = new fakeUser();
		user.assignRegister();		
		post.sendPost("http://fakerestapi.azurewebsites.net/api/Users","{\r\n" + 
				"\"ID\": 123123,\r\n" + 
				"\"UserName\": \""+user.getFirstName()+"\",\r\n" + 
				"\"Password\": \""+user.getPassword()+"\"\r\n" + 
				"}");

	}

	@When("^observo status code$")
	public void observo_status_code() throws Throwable {
	    result = post.getStatus();

	}

	@Then("^status code deve ser duzentos$")
	public void status_code_deve_ser_duzentos() throws Throwable {
	    if(result!=200) {
	    	throw new PendingException();
	    }
	}
}
