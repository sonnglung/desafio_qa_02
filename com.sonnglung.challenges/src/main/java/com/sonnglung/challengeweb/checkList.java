/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sonnglung.challengeweb;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author GUI
 */
public class checkList {
    private String firstName;
    private String lastName;
    public WebDriver driver;

    public checkList(WebDriver driver){
        this.driver = driver;
    }

    public checkList(){
    }    
    
    public WebDriver getDriver(){
        driverManager manager = new driverManager();
        manager.assignDriver(false);
        this.driver = manager.getDriver();
        return driver;
    }    
    
    public void assignName(String first,String last){
        this.firstName = first;
        this.lastName = last;
    }
    
    public boolean chekInList(){
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        this.driver.get("https://www.phptravels.net/admin/accounts/suppliers");
        String compFName = this.driver.findElement(By.xpath("//table//tbody//tr/td[3]")).getText();
        String compLName = this.driver.findElement(By.xpath("//table//tbody//tr/td[4]")).getText();
        if(compFName.equals(this.firstName)&&(compLName.equals(this.lastName))){
            return true;
        }
        else{
            return false;
        }
        
    }
}
