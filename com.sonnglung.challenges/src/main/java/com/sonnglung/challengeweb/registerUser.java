/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sonnglung.challengeweb;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author GUI
 */
public class registerUser {
    fakeUser user =  new fakeUser();
        
    public WebDriver driver;
    
    public registerUser(WebDriver driver){
        this.driver = driver;
    }

    public registerUser(){
    }        
    public WebDriver getDriver(){
        driverManager manager = new driverManager();
        manager.assignDriver(false);
        this.driver = manager.getDriver();
        return driver;
    }
    
    public void fillFields(){
        //TimeUnit.SECONDS.sleep(2);
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        this.driver.get("https://www.phptravels.net/admin/accounts/suppliers/add");
        //TimeUnit.SECONDS.sleep(10);
      
        //fakeUser user = new fakeUser();
        this.user.assignRegister();
        this.driver.findElement(By.xpath("//input[@name='fname']")).sendKeys(user.getFirstName());
        this.driver.findElement(By.xpath("//input[@name='lname']")).sendKeys(user.getLastName());
        this.driver.findElement(By.xpath("//input[@name='email']")).sendKeys(user.getEmailAddress());
        this.driver.findElement(By.xpath("//input[@name='password']")).sendKeys(user.getPassword());
        this.driver.findElement(By.xpath("//input[@name='mobile']")).sendKeys(user.getPhoneNumber());
        this.driver.findElement(By.xpath("//input[@name='address1']")).sendKeys(user.getStreetAddress());
        this.driver.findElement(By.xpath("//input[@name='address2']")).sendKeys(user.getStreetAddress());
        this.driver.findElement(By.xpath("//span[@class='select2-arrow']")).click();
        WebElement countryField = this.driver.findElement(By.xpath("//div[@class='select2-drop select2-display-none select2-with-searchbox select2-drop-active']//input"));
        try {
        	countryField.sendKeys(user.getCountry());
        	countryField.sendKeys(Keys.ENTER);}
        catch(Exception e) {
            countryField.sendKeys("Brazil");
            countryField.sendKeys(Keys.ENTER);        	
        }
    }
    
    public void submitRegister(){
        this.driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
    }
    
    public String getFirstName(){
        return this.user.getFirstName();
    }
    
    public String getLastName(){
        return this.user.getLastName();
    }
}
