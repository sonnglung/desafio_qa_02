/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sonnglung.challengeweb;

import com.github.javafaker.Faker;

/**
 *
 * @author GUI
 */

public class run {
    
    public static void main(String[] args){
        //=================================
        //  L O G I N
        //=================================
        pageLogin myLogin;         
        myLogin= new pageLogin();
        registerUser regist = new registerUser(myLogin.getDriver());
        myLogin.executeLogin("admin@phptravels.com","demoadmin");
        //=================================
        //  R E G I S T E R
        //=================================
        regist.fillFields();
        regist.submitRegister();
        //=================================
        //  C H E C K 
        //=================================
        String userFName = regist.getFirstName();
        String userLName = regist.getLastName();
        System.out.println(userFName);
        System.out.println(userLName);
        checkList check = new checkList(regist.driver);
        check.assignName(userFName, userLName);
        boolean result = check.chekInList();
        System.out.println(result);
    }
    
}
