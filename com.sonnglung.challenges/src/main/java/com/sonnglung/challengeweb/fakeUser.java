/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sonnglung.challengeweb;

import com.github.javafaker.Faker;

/**
 *
 * @author GUI
 */
public class fakeUser {
    private String firstName;
    private String lastName;
    private String password;
    private String streetAddress;
    private String emailAddress;
    private String country;
    private String phoneNumber;
    
    public void assignRegister(){
        Faker faker = new Faker();
        this.firstName = faker.name().firstName();
        this.lastName = faker.name().lastName();
        this.password = faker.pokemon().name();
        this.streetAddress = faker.address().streetAddress();
        this.emailAddress = faker.internet().emailAddress();
        this.country = faker.address().country();
        this.phoneNumber = faker.phoneNumber().cellPhone();
    }
    
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public String getPassword(){
        return password;
    }
    public String getStreetAddress(){
        return streetAddress;
    }
    public String getEmailAddress(){
        return emailAddress;
    }
    public String getCountry(){
        return country;
    }
    public String getPhoneNumber(){
        return phoneNumber;
    }
    
}
