/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sonnglung.challengeweb;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


/**
 *
 * @author GUI
 */
public class pageLogin {
    public WebDriver driver;
    
    public pageLogin(WebDriver driver){
        this.driver = driver;
    }

    public pageLogin(){
    }    
    
    public WebDriver getDriver(){
        driverManager manager = new driverManager();
        manager.assignDriver(false);
        this.driver = manager.getDriver();
        return driver;
    }
    
    public void executeLogin(String user,String password){
        this.driver.get("https://www.phptravels.net/admin");
        this.driver.findElement(By.xpath("//input[@name='email']")).sendKeys(user);
        WebElement passw = this.driver.findElement(By.xpath("//input[@name='password']"));
        passw.sendKeys(password);
        passw.submit();
        
    }

}