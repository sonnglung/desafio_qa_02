/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sonnglung.challengeweb;

import java.util.Scanner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author GUI
 */
public class driverManager {
    private WebDriver driver;
    
    public void assignDriver(Boolean switching){
        if(switching){
            Scanner driverInput = new Scanner(System.in);
            String driverName = driverInput.nextLine();
            if(driverName.equals("Firefox")){
                System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
                this.driver = new FirefoxDriver();
            }
            if(driverName.equals("Chrome")){
                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
                this.driver = new ChromeDriver();
            }
        }
        else{
            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
            this.driver = new FirefoxDriver();
        }
    }
    
    public WebDriver getDriver(){
        return driver;
    }
    
}
